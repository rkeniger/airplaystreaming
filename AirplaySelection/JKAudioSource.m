//
//  JKAudioSource.m
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/15/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <CoreAudio/CoreAudio.h>

#import "JKAudioSource.h"
#import "JKAudioDevice.h"

@implementation JKAudioSource

- (id) initWithID:(UInt32)sourceID device:(JKAudioDevice *)device {
	self = [super init];
	
	if (self) {
		_sourceID = sourceID;
		_device = device;
		
		AudioObjectPropertyAddress nameAddr;
		nameAddr.mSelector = kAudioDevicePropertyDataSourceNameForIDCFString;
		nameAddr.mScope = kAudioObjectPropertyScopeOutput;
		nameAddr.mElement = kAudioObjectPropertyElementMaster;
		
		CFStringRef value = NULL;
		
		AudioValueTranslation audioValueTranslation;
		audioValueTranslation.mInputDataSize = sizeof(UInt32);
		audioValueTranslation.mOutputData = (void *) &value;
		audioValueTranslation.mOutputDataSize = sizeof(CFStringRef);
		audioValueTranslation.mInputData = (void *) &_sourceID;
		
		UInt32 propsize = sizeof(AudioValueTranslation);
		
		AudioObjectGetPropertyData(_device.deviceID, &nameAddr, 0, NULL, &propsize, &audioValueTranslation);
		
		_name = (__bridge NSString *)value;
	}
	
	return self;
}

- (NSString *) description {
	return [NSString stringWithFormat:@"JKAudioSource: %@", self.name];
}

@end
