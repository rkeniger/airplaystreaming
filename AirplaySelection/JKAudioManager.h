//
//  JKAudioManager.h
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/14/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKAudioManager : NSObject

+ (JKAudioManager *) sharedManager;

- (NSArray *) outputDevices;

@end
