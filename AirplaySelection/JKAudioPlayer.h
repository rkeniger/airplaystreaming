//
//  JKAudioPlayer.h
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/15/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/CoreAudio.h>

@interface JKAudioPlayer : NSObject

- (id) initWithURL:(NSURL *)url;

- (BOOL) isPlaying;

- (void) play;
- (void) pause;
- (void) stop;

- (void) setOutputAudioDevice:(NSString *)audioDeviceUID;

@end
