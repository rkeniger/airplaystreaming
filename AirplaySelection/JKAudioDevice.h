//
//  JKAudioDevice.h
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/14/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/CoreAudio.h>

#import "JKAudioSource.h"

@interface JKAudioDevice : NSObject

@property(nonatomic, readonly) AudioDeviceID deviceID;
@property(nonatomic, readonly) BOOL isInput;
@property(nonatomic, readonly) BOOL isAirplay;
@property(nonatomic, readonly) NSString *identifier;

- (id) initWithDeviceID:(AudioDeviceID)deviceID;

- (NSString *) name;
- (NSArray *) audioSources;
- (void) setAudioSource:(JKAudioSource *)audioSource;
- (void) setAudioSources:(NSArray *)sources;


@end
