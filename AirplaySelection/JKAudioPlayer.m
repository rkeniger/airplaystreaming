//
//  JKAudioPlayer.m
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/15/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import "JKAudioPlayer.h"

#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>

@interface JKAudioPlayer ()
- (void) callbackForBuffer:(AudioQueueBufferRef)buffer;
@end

static void BufferCallback(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef buffer) {
	// redirect back to the class to handle it there instead, so we have direct access to the instance variables
	[(__bridge JKAudioPlayer *)inUserData callbackForBuffer:buffer];
}

static UInt32 gBufferSizeBytes = 0x10000; // 64k
#define NUM_QUEUE_BUFFERS	3

@implementation JKAudioPlayer {
	AudioFileID audioFile;
	AudioQueueRef queue;
	AudioStreamBasicDescription dataFormat;
	
	UInt64 packetIndex;
	UInt32 numPacketsToRead;
	AudioStreamPacketDescription *packetDescs;
	BOOL repeat;
	BOOL trackClosed;
	AudioQueueBufferRef buffers[NUM_QUEUE_BUFFERS];
	
	BOOL isPlaying;
}

- (id) initWithURL:(NSURL *)url {
	self = [super init];
	
	if (self) {
		UInt32 size, maxPacketSize;
		char *cookie;
		
		isPlaying = NO;
		
		OSStatus status = AudioFileOpenURL((__bridge CFURLRef)url, 0x01, kAudioFileM4AType, &audioFile);
		if (status != noErr) {
			NSLog(@"Failed to open audio file");
			return nil;
		}
		
		size = sizeof(dataFormat);
		AudioFileGetProperty(audioFile, kAudioFilePropertyDataFormat, &size, &dataFormat);
		
		AudioQueueNewOutput(&dataFormat, BufferCallback, (__bridge void *)self, nil, nil, 0, &queue);
		
		if (dataFormat.mBytesPerPacket == 0 || dataFormat.mFramesPerPacket == 0) {
			// since we didn't get sizes to work with, then this must be VBR data (Variable BitRate), so
			// we'll have to ask Core Audio to give us a conservative estimate of the largest packet we are
			// likely to read with kAudioFilePropertyPacketSizeUpperBound
			size = sizeof(maxPacketSize);
			AudioFileGetProperty(audioFile, kAudioFilePropertyPacketSizeUpperBound, &size, &maxPacketSize);
			if (maxPacketSize > gBufferSizeBytes) {
				// hmm... well, we don't want to go over our buffer size, so we'll have to limit it I guess
				maxPacketSize = gBufferSizeBytes;
				NSLog(@"GBMusicTrack Warning - initWithPath: had to limit packet size requested for file path: %@", url);
			}
			numPacketsToRead = gBufferSizeBytes / maxPacketSize;
			
			// will need a packet description for each packet since this is VBR data, so allocate space accordingly
			packetDescs = malloc(sizeof(AudioStreamPacketDescription) * numPacketsToRead);
		} else {
			// for CBR data (Constant BitRate), we can simply fill each buffer with as many packets as will fit
			numPacketsToRead = gBufferSizeBytes / dataFormat.mBytesPerPacket;
			
			// don't need packet descriptsions for CBR data
			packetDescs = nil;
		}
		
		// see if file uses a magic cookie (a magic cookie is meta data which some formats use)
		AudioFileGetPropertyInfo(audioFile, kAudioFilePropertyMagicCookieData, &size, nil);
		if (size > 0)
		{
			// copy the cookie data from the file into the audio queue
			cookie = malloc(sizeof(char) * size);
			AudioFileGetProperty(audioFile, kAudioFilePropertyMagicCookieData, &size, cookie);
			AudioQueueSetProperty(queue, kAudioQueueProperty_MagicCookie, cookie, size);
			free(cookie);
		}
		
		packetIndex = 0;
		
		repeat = NO;
		trackClosed = NO;
	}
	
	return self;
}

- (BOOL) isPlaying {
	return isPlaying;
}

- (void) play {
	NSLog(@"Play!");
	
	// allocate and prime buffers with some data
	for (int i = 0; i < NUM_QUEUE_BUFFERS; i++) {
		AudioQueueAllocateBuffer(queue, gBufferSizeBytes, &buffers[i]);
		if ([self readPacketsIntoBuffer:buffers[i]] == 0)
		{
			// this might happen if the file was so short that it needed less buffers than we planned on using
			break;
		}
	}
	
	AudioQueueStart(queue, nil);
	isPlaying = YES;
}

- (void) pause {
	AudioQueueStop(queue, YES);
	isPlaying = NO;
	
	packetIndex = MAX(packetIndex - numPacketsToRead, 0);
}

- (void) stop {
	AudioQueueStop(queue, YES);
	isPlaying = NO;
	
	// reset for replay
	packetIndex = 0;
}

- (void) setOutputAudioDevice:(NSString *)audioDeviceUID {
	NSLog(@"Identifier: %@", audioDeviceUID);
	
	CFStringRef deviceUID = (__bridge CFStringRef) audioDeviceUID;
	
	OSStatus status = AudioQueueSetProperty(queue, kAudioQueueProperty_CurrentDevice, &deviceUID, sizeof(audioDeviceUID));
	if (status != noErr) {
		NSLog(@"Error setting current audio device: %d", status);
		
		if (status == kAudioQueueErr_InvalidRunState) {
			NSLog(@"Invalid run state");
		}
	}
}

#pragma mark - Callback

- (void)callbackForBuffer:(AudioQueueBufferRef)buffer {
	if ([self readPacketsIntoBuffer:buffer] == 0) {
		// End Of File reached, so rewind and refill the buffer using the beginning of the file instead
		packetIndex = 0;
		[self readPacketsIntoBuffer:buffer];
	}
}

- (UInt32)readPacketsIntoBuffer:(AudioQueueBufferRef)buffer {
	UInt32 numBytes = 0, numPackets = 0;
	
	numPackets = numPacketsToRead;
	AudioFileReadPackets(audioFile, NO, &numBytes, packetDescs, packetIndex, &numPackets, buffer->mAudioData);
	
	if (numPackets > 0) {
		// - End Of File has not been reached yet since we read some packets, so enqueue the buffer we just read into
		// the audio queue, to be played next
		// - (packetDescs ? numPackets : 0) means that if there are packet descriptions (which are used only for Variable
		// BitRate data (VBR)) we'll have to send one for each packet, otherwise zero
		buffer->mAudioDataByteSize = numBytes;
		AudioQueueEnqueueBuffer(queue, buffer, (packetDescs ? numPackets : 0), packetDescs);
		
		// move ahead to be ready for next time we need to read from the file
		packetIndex += numPackets;
	}
	
	return numPackets;
}

@end
