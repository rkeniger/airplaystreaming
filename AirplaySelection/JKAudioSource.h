//
//  JKAudioSource.h
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/15/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JKAudioDevice;

@interface JKAudioSource : NSObject

@property(nonatomic, readonly) UInt32 sourceID;
@property(nonatomic, readonly) JKAudioDevice *device;
@property(nonatomic, readonly) NSString *name;

- (id) initWithID:(UInt32)sourceID device:(JKAudioDevice *)device;

@end
