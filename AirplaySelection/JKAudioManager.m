//
//  JKAudioManager.m
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/14/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <CoreAudio/CoreAudio.h>

#import "JKAudioManager.h"
#import "JKAudioDevice.h"

@interface JKAudioManager ()
- (void) browseDevices;
@end

@implementation JKAudioManager {
	NSMutableArray *_outputDevices;
}

+ (JKAudioManager *) sharedManager {
	static JKAudioManager *sharedInstance;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[JKAudioManager alloc] init];
	});
	
	return sharedInstance;
}

- (id) init {
	self = [super init];
	
	if (self) {
		_outputDevices = [NSMutableArray array];
		[self browseDevices];
	}
	
	return self;
}

- (void) browseDevices {
	[_outputDevices removeAllObjects];
	
	AudioObjectPropertyAddress addr;
	addr.mSelector = kAudioHardwarePropertyDevices;
	addr.mScope = kAudioObjectPropertyScopeWildcard;
	addr.mElement = kAudioObjectPropertyElementWildcard;
	
	UInt32 propsize;
	AudioObjectGetPropertyDataSize(kAudioObjectSystemObject, &addr, 0, NULL, &propsize);
	
	int nDevices = propsize / sizeof(AudioDeviceID);
	
	AudioDeviceID *devids = malloc(propsize);
	AudioObjectGetPropertyData(kAudioObjectSystemObject, &addr, 0, NULL, &propsize, devids);
	
	for (int i=0; i<nDevices; i++) {
		JKAudioDevice *device = [[JKAudioDevice alloc] initWithDeviceID:devids[i]];
		
		if (![device isInput]) {
			[_outputDevices addObject:device];
		}
	}
	
	free(devids);
}

- (NSArray *) outputDevices {
	return _outputDevices;
}

@end
